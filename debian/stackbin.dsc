Format: 3.0 (quilt)
Source: stackbin
Binary: stackbin
Architecture: all
Version: 0.0.2-1
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://bgstack15.ddns.net/
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12)
Package-List:
 stackbin deb web optional arch=all
Files:
 00000000000000000000000000000000 1 stackbin.orig.tar.gz
 00000000000000000000000000000000 1 stackbin.debian.tar.xz
