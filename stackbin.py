# File: stackbin.py
# Location: http://gitlab.com/bgstack15/stackbin/
# Authors: mitsuhiko, ofshellohicy, su27, bgstack15
# SPDX-License-Identifier: GPL-3.0
# Startdate: 2011 by mitsuhiko
# Title: Stackbin
# Purpose: Flask-based pastebin
# History:
#    2014 ofshellohicy removed some features
#    2016 su27 added some features
#    2022 bgstack15 hard forked
#    2022-03-17 add support for command | curl -d '@-'
# Reference:
#    fuss.py
#    https://stackoverflow.com/questions/15974730/how-do-i-get-the-different-parts-of-a-flask-requests-url
# Improve:
# Dependencies:
#    dep-devuan: python3-pytimeparse, python3-uwsgidecorators, python3-flask, python3:any, uwsgi-core, uwsgi-plugin-python3
#    dep-fedora: uwsgi, uwsgi-logger-file, python3-flask, uwsgi-plugin-python3, python3-sqlalchemy, python3-uwsgidecorators, python3-pytimeparse, python3-uwsgidecorators
#    dep-centos7: uwsgi, uwsgi-logger-file, python36-flask, uwsgi-plugin-python36, python36-sqlalchemy, python36-uwsgidecorators
#    pip-centos7: flask-sqlalchemy, pytimeparse
# Documentation: see README.md
from datetime import datetime, timedelta
from itsdangerous import Signer
from flask import Blueprint, Flask, Response, request, url_for, redirect, g, render_template, session, abort, current_app
from flask_sqlalchemy import SQLAlchemy
from werkzeug.middleware.proxy_fix import ProxyFix
from pytimeparse.timeparse import timeparse # python3-pytimeparse
import os, sys
# uwsgidecorators load will fail when using initdb.py but is also not necessary
try:
   from uwsgidecorators import timer # python3-uwsgidecorators
except:
   print("Warning! Without uwsgidecorators, the cleanup timer cannot run.")
import time

sys.path.append('/usr/libexec/stackbin')
from stackbin_auth import auth, requires_session

from sqlalchemy import types
from sqlalchemy.dialects.mysql.base import MSBinary
from sqlalchemy.schema import Column
import uuid
class UUID(types.TypeDecorator):
   impl = MSBinary
   cache_ok = True
   def __init__(self):
      self.impl.length = 16
      self.cache_ok = False # to shut up some useless warning
      types.TypeDecorator.__init__(self,length=self.impl.length)
   def process_bind_param(self,value,dialect=None):
      if value and isinstance(value,uuid.UUID):
         return value.bytes
      elif value and not isinstance(value,uuid.UUID):
         raise ValueError('value %s is not a valid uuid.UUID' % value)
      else:
         return None
   def process_result_value(self,value,dialect=None):
      if value:
         return uuid.UUID(bytes=value)
      else:
         return None
   def is_mutable(self):
      return False
id_column_name = "id"
def id_column():
    return Column(id_column_name,UUID(),primary_key=True,default=uuid.uuid4)

def get_signed(string, salt="blank"):
   return Signer(app.secret_key, salt=salt).sign(str(string))

def get_unsigned(string, salt="blank"):
   return Signer(app.secret_key, salt=salt).unsign(str(string)).decode("utf-8")

app = Flask(__name__)
app.register_blueprint(auth) # url_prefix='/'
try:
   app.config.from_pyfile(os.environ['STACKBIN_CONF'])
except:
   app.config.from_pyfile('stackbin.conf')
db = SQLAlchemy(app)
if "STATIC_FOLDER" in app.config:
   app.static_folder=app.config["STATIC_FOLDER"]
if "TEMPLATE_FOLDER" in app.config:
   app.template_folder=app.config["TEMPLATE_FOLDER"]

def url_for_other_page(page):
   args = request.view_args.copy()
   args['page'] = page
   return url_for(request.endpoint, **args)
app.jinja_env.globals['url_for_other_page'] = url_for_other_page
app.jinja_env.globals['appname'] = app.config['APPNAME']

def refresh_string(delay,url):
   """
   Returns a string for html content for redirecting the user back after the
   requested delay, to the requested url.
   """
   return f'<meta http-equiv="Refresh" content="{delay}; url={url}">'

@app.before_first_request
def visit_set():
   if 'SET_URL' in app.config and '/set' in app.config['SET_URL']:
      os.system(f"curl {app.config['SET_URL']} 1>/dev/null 2>&1 &")

@app.before_request
def check_user_status():
   g.user = None
   if 'user_id' in session:
      #g.user = User.query.get(session['user_id'])
      g.user = session['user_id']

class Paste(db.Model):
   id = id_column()
   code = db.Column(db.Text)
   title = db.Column(db.Text)
   pub_date = db.Column(db.DateTime)
   exp_date = db.Column(db.DateTime)
   user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
   is_private = db.Column(db.Boolean)
   parent_id = db.Column(UUID(), db.ForeignKey('paste.id'))
   parent = db.relationship('Paste', lazy=True, backref='children', uselist=False, remote_side=[id])

   def __init__(self, user, code, title, relative_expiration_seconds, parent=None, is_private=False):
      # add new user if necessary
      if user and user != "None":
         try:
            found_user = User.query.filter_by(display_name=user).all()[0]
         except:
            found_user = User(display_name=user)
         self.user = found_user
      self.code = code
      self.title = title
      self.is_private = is_private
      u = datetime.utcnow()
      try:
         # this will fail on POSTed value of "never" for exp
         b = timedelta(seconds=relative_expiration_seconds)
      except:
         # so timedelta() will return 0 seconds, which makes the exp_date = pub_date, which is treated
         # by the cleanup logic and jinja2 templates as never-expires.
         b = timedelta()
      self.pub_date = u
      self.exp_date = u + b
      self.parent = parent

class User(db.Model):
   id = db.Column(db.Integer, primary_key=True)
   display_name = db.Column(db.String(120))
   #fb_id = db.Column(db.String(30), unique=True)
   pastes = db.relationship(Paste, lazy='dynamic', backref='user')

   def __init__(self, display_name = None):
      self.display_name = display_name
      db.session.add(self)
      db.session.commit()

def _calculate_expiration(config, request = None, default = 0):
   """
   Given the Flask request, default expiration value, and app.config, calculate this specific expiration timestamp for a paste. This is called by new_paste().
   """
   exp = default
   if 'exp' in request.form and request.form['exp']:
      exp_opt = request.form['exp']
      if exp_opt not in config['EXPIRATION_OPTIONS']:
         try:
            exp = timeparse(f"+ {config['EXPIRATION_OPTIONS'][0]}")
         except:
            exp = 60 * 60 # failsafe, 1 hour
         print(f"WARNING: requested expiration \"{exp_opt}\" is not in the list of options {config['EXPIRATION_OPTIONS']}, so will use {exp}")
      else:
         try:
            exp = timeparse(f"+ {exp_opt}")
         except:
            exp = 0
   else:
      # so if the exp was not in the request form, i.e., this was from curl -d '@-', then use first option
      try:
         exp = timeparse(f"+ {config['EXPIRATION_OPTIONS'][0]}")
      except:
         exp = 60 * 60 # failsafe, 1 hour
   return exp

@app.route('/', methods=['GET', 'POST'])
def new_paste():
   parent = None
   reply_to = request.args.get('reply_to')
   if reply_to is not None:
      try:
         parent = Paste.query.get(uuid.UUID(reply_to))
      except:
         parent = Paste.query.get(reply_to)
   if request.method == 'POST':
      # This request.get_data() must be here in order for the else contents = to work.
      # It is probably related to getting the data before checking for request.form or something.
      request.get_data()
      if 'code' in request.form and request.form['code']:
         is_private = bool(request.form.get('is_private') or False)
         title = "Untitled paste"
         if request.form['pastetitle'] and request.form['pastetitle'] != "Enter title here":
            title = request.form['pastetitle']
         relative_expiration_seconds = 0
         exp = _calculate_expiration(app.config, request, 0)
         paste = Paste(
            g.user,
            request.form['code'],
            title,
            exp,
            parent=parent,
            is_private=is_private
         )
         db.session.add(paste)
         db.session.commit()
         sign = get_signed(paste.id, salt=app.config['SALT']) \
            if is_private else None
         return redirect(url_for('show_paste', paste_id=paste.id, s=sign))
      else:
         # form field 'code' was not present, so this is probably command line piped into curl
         contents = request.get_data().decode('utf8')
         first_line = contents.split('\n')[0]
         # cli does not provide choices for expiration, so use the first option from the list in the app config.
         exp = _calculate_expiration(app.config, request, 0)
         paste = Paste(
            g.user,
            contents,
            first_line,
            exp,
            parent = None,
            is_private = False
         )
         # the url_root makes it a complete url
         db.session.add(paste)
         db.session.commit()
         # We cannot rely on a user having visited /set already, so let us parse the proxied values right now.
         prefix = ''
         host = request.url_root
         if 'HTTP_X_FORWARDED_PREFIX' in request.environ and 'PREFIX' not in app.config:
            prefix = ''.join(request.environ['HTTP_X_FORWARDED_PREFIX'].split(", "))
         if 'HTTP_X_FORWARDED_HOST' in request.environ:
            #request.environ['wsgi.url_scheme'] tends to always be http
            protocol = "https"
            if 'CURL_RESPONSE_PROTOCOL' in app.config:
               protocol = app.config['CURL_RESPONSE_PROTOCOL']
            host = protocol + "://" + request.environ['HTTP_X_FORWARDED_HOST'].split(", ")[0]
         return host.strip('/') + prefix.rstrip('/') + url_for('show_paste', paste_id = paste.id) + "\n"
   # and now back to the GET
   try:
      exp_opts = app.config['EXPIRATION_OPTIONS']
   except:
      exp_opts = None
   return render_template('new_paste.html', parent=parent, exp_opts = exp_opts)

# This @timer is from the uwsgidecorators
try:
   @timer(app.config['LOOP_DELAY'])
   def cleanup_expired_pastes(num):
      # num is useless.
      """
      Every LOOP_DELAY seconds, find any entries that have expired and then delete them.
      """
      all1 = Paste.query.all()
      need_commit = False
      for p in all1:
         # the exp_date != pub_date is very important, because anything with "never" expires
         # is stored in the db as exp_date = pub_date
         if p.exp_date and p.exp_date != p.pub_date and p.exp_date <= datetime.utcnow():
            print(f"INFO: deleting paste \"{p.title}\" with expiration {p.exp_date}.")
            Paste.query.filter(Paste.id == p.id).delete()
            need_commit = True
      # only run the commit once!
      if need_commit:
         db.session.commit()
except:
   pass

@app.route('/<paste_id>/')
@app.route('/<paste_id>')
def show_paste(paste_id):
   try:
      paste = Paste.query.options(db.eagerload('children')).get_or_404(paste_id)
   except:
      paste = Paste.query.options(db.eagerload('children')).get_or_404(uuid.UUID(paste_id))
   if paste.is_private:
      try:
         sign = request.args.get('s', '')
         assert str(paste.id) == \
            get_unsigned(sign, salt=app.config['SALT'])
      except:
         abort(403)
   parent = None
   if paste.parent_id:
      try:
         parent = Paste.query.get(uuid.UUID(paste.parent_id))
      except:
         parent = Paste.query.get(paste.parent_id)
   children = []
   if paste.children:
      for i in paste.children:
         j = None
         try:
            j = Paste.query.get(uuid.UUID(i.id))
         except:
            j = Paste.query.get(i.id)
         if j:
            k = j.id, j.title
            children.append(k)
   return render_template('show_paste.html', paste=paste, parent=parent, children=children)

@app.route('/<paste_id>/delete/', methods=['POST'])
@app.route('/<paste_id>/delete', methods=['POST'])
def delete_paste(paste_id):
   try:
      paste = Paste.query.options(db.eagerload('children')).get_or_404(paste_id)
   except:
      paste = Paste.query.options(db.eagerload('children')).get_or_404(uuid.UUID(paste_id))
   sign = str(request.form['s'])
   try:
      assert str(paste.id) == get_unsigned(sign, salt=app.config['DELETESALT'])
   except:
      abort(403)
   try:
      Paste.query.filter(Paste.id == paste.id).delete()
      db.session.commit()
      message = refresh_string(1, url_for("admin")) + "OK"
      return message,200
   except:
      return f"failure to delete object. Select <a href='{url_for('admin')}'>here</a> to return to the admin panel.",500

def get_all_pastes():
   """
   Get custom arrangement of pastes for Admin view
   """
   all1 = Paste.query.all()
   all2 = []
   for p1 in all1:
      parent_id = None
      parent_title = None
      children = []
      if p1.parent_id:
         parent_id = p1.parent_id
         try:
            parent_title = Paste.query.get(p1.parent_id).title
         except:
            parent_title = "" # works better than None for the parent column of the generated html
      if p1.children:
         for c1 in p1.children:
            child = Paste.query.get(c1.id)
            child_title = child.title
            c2 = c1.id, child_title
            children.append(c2)
      private = None
      if p1.is_private:
         private = get_signed(p1.id, salt=app.config['SALT'])
      user_id = ""
      if p1.user_id:
         user_id = User.query.get(p1.user_id).display_name
      p2 = {
         "id": p1.id,
         "title": p1.title,
         "pub_date": p1.pub_date,
         "exp_date": p1.exp_date,
         "private": private,
         "user": user_id,
         "is_private": p1.is_private,
         "parent": (parent_id, parent_title),
         "children": children,
         "delete": get_signed(p1.id, salt=app.config['DELETESALT']).decode("utf-8")
      }
      all2.append(p2)
   return all2

@app.route('/admin/')
@app.route('/admin')
@requires_session
def admin(username, groups = []):
   all_pastes = get_all_pastes()
   return render_template('admin.html', pastes = all_pastes)

@app.route('/favicon.ico')
def favicon():
   try:
      return redirect(url_for('static', filename='favicon.ico'))
   except:
      abort(404)

@app.route('/set')
def get_proxied_path():
   prefix = "/"
   #print(f"DEBUG: request.environ: {request.environ}")
   if 'HTTP_X_FORWARDED_PREFIX' in request.environ:
      hl = len(dict(request.headers)["X-Forwarded-Host"].split(", "))
      pl = len(dict(request.headers)["X-Forwarded-Prefix"].split(", "))
      prefix = request.environ['HTTP_X_FORWARDED_PREFIX']
      app.wsgi_app = ProxyFix(app.wsgi_app,x_for=pl,x_host=hl,x_port=hl,x_prefix=pl,x_proto=pl)
      app.config['PROXYFIX'] = pl
      app.config['PREFIX'] = prefix
   # we can afford to use prefix because new_paste is the top-level endpoint of the whole app
   message = refresh_string(1, (prefix + "/").replace("//","/")) + "OK"
   return message, 200

# stubs, to simplify any templates that ask url_for("login")
@app.route('/login/')
def login(user="None"): True
@app.route("/logout/")
def logout(): True

# Initialize the database if it does not already exist
db.create_all()
if __name__ == "__main__":
   app.run()
